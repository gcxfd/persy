use crate::error::PERes;
use byteorder::{BigEndian, ByteOrder, ReadBytesExt, WriteBytesExt};
use std::io::{Cursor, Read, Write};
use unsigned_varint::{encode, io};
use zigzag::ZigZag;

pub trait InfallibleRead {
    fn read_exact(&mut self, buf: &mut [u8]);
}

pub trait InfallibleWrite {
    fn write_all(&mut self, buf: &[u8]);
}

struct InfallibleReadWrapper<'a, T: InfallibleRead + ?Sized> {
    reader: &'a mut T,
}

#[allow(dead_code)]
impl<'a, T: InfallibleRead + ?Sized> InfallibleReadWrapper<'a, T> {
    fn new(reader: &'a mut T) -> Self {
        InfallibleReadWrapper { reader }
    }
}
impl<'a, T: InfallibleRead + ?Sized> Read for InfallibleReadWrapper<'a, T> {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        InfallibleRead::read_exact(self.reader, buf);
        Ok(buf.len())
    }
}

// End of snippet taken from integer-ecoding specialized for InfallibleRead

pub(crate) trait InfallibleReadVarInt: InfallibleRead {
    #[inline]
    fn read_varint_u8(&mut self) -> u8 {
        io::read_u8(InfallibleReadWrapper::new(self)).expect("infallible")
    }

    #[inline]
    fn read_varint_u16(&mut self) -> u16 {
        io::read_u16(InfallibleReadWrapper::new(self)).expect("infallible")
    }

    #[inline]
    fn read_varint_u32(&mut self) -> u32 {
        io::read_u32(InfallibleReadWrapper::new(self)).expect("infallible")
    }

    #[inline]
    fn read_varint_u64(&mut self) -> u64 {
        io::read_u64(InfallibleReadWrapper::new(self)).expect("infallible")
    }

    #[inline]
    fn read_varint_u128(&mut self) -> u128 {
        io::read_u128(InfallibleReadWrapper::new(self)).expect("infallible")
    }

    #[inline]
    fn read_varint_i8(&mut self) -> i8 {
        ZigZag::decode(self.read_varint_u8())
    }

    #[inline]
    fn read_varint_i16(&mut self) -> i16 {
        ZigZag::decode(self.read_varint_u16())
    }

    #[inline]
    fn read_varint_i32(&mut self) -> i32 {
        ZigZag::decode(self.read_varint_u32())
    }

    #[inline]
    fn read_varint_i64(&mut self) -> i64 {
        ZigZag::decode(self.read_varint_u64())
    }

    #[inline]
    fn read_varint_i128(&mut self) -> i128 {
        ZigZag::decode(self.read_varint_u128())
    }
}
impl<R: InfallibleRead + ?Sized> InfallibleReadVarInt for R {}

pub(crate) trait InfallibleWriteVarInt: InfallibleWrite {
    #[inline]
    fn write_varint_u8(&mut self, value: u8) {
        self.write_all(encode::u8(value, &mut encode::u8_buffer()));
    }

    #[inline]
    fn write_varint_u16(&mut self, value: u16) {
        self.write_all(encode::u16(value, &mut encode::u16_buffer()));
    }

    #[inline]
    fn write_varint_u32(&mut self, value: u32) {
        self.write_all(encode::u32(value, &mut encode::u32_buffer()));
    }

    #[inline]
    fn write_varint_u64(&mut self, value: u64) {
        self.write_all(encode::u64(value, &mut encode::u64_buffer()));
    }

    #[inline]
    fn write_varint_u128(&mut self, value: u128) {
        self.write_all(encode::u128(value, &mut encode::u128_buffer()));
    }

    #[inline]
    fn write_varint_i8(&mut self, value: i8) {
        self.write_varint_u8(ZigZag::encode(value))
    }

    #[inline]
    fn write_varint_i16(&mut self, value: i16) {
        self.write_varint_u16(ZigZag::encode(value))
    }

    #[inline]
    fn write_varint_i32(&mut self, value: i32) {
        self.write_varint_u32(ZigZag::encode(value))
    }

    #[inline]
    fn write_varint_i64(&mut self, value: i64) {
        self.write_varint_u64(ZigZag::encode(value))
    }

    #[inline]
    fn write_varint_i128(&mut self, value: i128) {
        self.write_varint_u128(ZigZag::encode(value))
    }
}

impl<W: InfallibleWrite + ?Sized> InfallibleWriteVarInt for W {}

pub(crate) trait InfallibleWriteFormat: InfallibleWrite {
    #[inline]
    fn write_u8(&mut self, value: u8) {
        self.write_all(&[value])
    }

    #[inline]
    fn write_u16(&mut self, value: u16) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_u32(&mut self, value: u32) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_u64(&mut self, value: u64) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_u128(&mut self, value: u128) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_i8(&mut self, value: i8) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_i16(&mut self, value: i16) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_i32(&mut self, value: i32) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_i64(&mut self, value: i64) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_i128(&mut self, value: i128) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_f32(&mut self, value: f32) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_f64(&mut self, value: f64) {
        self.write_all(&value.to_be_bytes())
    }
}

impl<W: InfallibleWrite + ?Sized> InfallibleWriteFormat for W {}

impl InfallibleWrite for Vec<u8> {
    fn write_all(&mut self, buf: &[u8]) {
        self.extend_from_slice(buf);
    }
}

pub(crate) trait InfallibleReadFormat: InfallibleRead {
    #[inline]
    fn read_u8(&mut self) -> u8 {
        let mut value = [0; 1];
        self.read_exact(&mut value);
        value[0]
    }

    #[inline]
    fn read_u16(&mut self) -> u16 {
        let mut value = [0; 2];
        self.read_exact(&mut value);
        u16::from_be_bytes(value)
    }

    #[inline]
    fn read_u32(&mut self) -> u32 {
        let mut value = [0; 4];
        self.read_exact(&mut value);
        u32::from_be_bytes(value)
    }

    #[inline]
    fn read_u64(&mut self) -> u64 {
        let mut value = [0; 8];
        self.read_exact(&mut value);
        u64::from_be_bytes(value)
    }

    #[inline]
    fn read_u128(&mut self) -> u128 {
        let mut value = [0; 16];
        self.read_exact(&mut value);
        u128::from_be_bytes(value)
    }

    #[inline]
    fn read_i8(&mut self) -> i8 {
        let mut value = [0; 1];
        self.read_exact(&mut value);
        i8::from_be_bytes(value)
    }

    #[inline]
    fn read_i16(&mut self) -> i16 {
        let mut value = [0; 2];
        self.read_exact(&mut value);
        i16::from_be_bytes(value)
    }

    #[inline]
    fn read_i32(&mut self) -> i32 {
        let mut value = [0; 4];
        self.read_exact(&mut value);
        i32::from_be_bytes(value)
    }

    #[inline]
    fn read_i64(&mut self) -> i64 {
        let mut value = [0; 8];
        self.read_exact(&mut value);
        i64::from_be_bytes(value)
    }

    #[inline]
    fn read_i128(&mut self) -> i128 {
        let mut value = [0; 16];
        self.read_exact(&mut value);
        i128::from_be_bytes(value)
    }

    #[inline]
    fn read_f32(&mut self) -> f32 {
        let mut value = [0; 4];
        self.read_exact(&mut value);
        f32::from_be_bytes(value)
    }

    #[inline]
    fn read_f64(&mut self) -> f64 {
        let mut value = [0; 8];
        self.read_exact(&mut value);
        f64::from_be_bytes(value)
    }
}

impl<R: InfallibleRead + ?Sized> InfallibleReadFormat for R {}

impl<T> InfallibleRead for Cursor<T>
where
    T: AsRef<[u8]>,
{
    fn read_exact(&mut self, buf: &mut [u8]) {
        Read::read_exact(self, buf).expect("never fail in a in memory buffer");
    }
}

pub(crate) fn read_u16(buf: &[u8]) -> u16 {
    BigEndian::read_u16(buf)
}
pub(crate) fn write_u16(buf: &mut [u8], val: u16) {
    BigEndian::write_u16(buf, val)
}
pub(crate) fn read_u64(buf: &[u8]) -> u64 {
    BigEndian::read_u64(buf)
}
pub(crate) fn write_u64(buf: &mut [u8], val: u64) {
    BigEndian::write_u64(buf, val)
}

pub(crate) trait WriteFormat: Write {
    #[inline]
    fn write_u8(&mut self, value: u8) -> PERes<()> {
        Ok(WriteBytesExt::write_u8(self, value)?)
    }

    #[inline]
    fn write_u16(&mut self, value: u16) -> PERes<()> {
        Ok(WriteBytesExt::write_u16::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_u32(&mut self, value: u32) -> PERes<()> {
        Ok(WriteBytesExt::write_u32::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_u64(&mut self, value: u64) -> PERes<()> {
        Ok(WriteBytesExt::write_u64::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_u128(&mut self, value: u128) -> PERes<()> {
        Ok(WriteBytesExt::write_u128::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_i8(&mut self, value: i8) -> PERes<()> {
        Ok(WriteBytesExt::write_i8(self, value)?)
    }

    #[inline]
    fn write_i16(&mut self, value: i16) -> PERes<()> {
        Ok(WriteBytesExt::write_i16::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_i32(&mut self, value: i32) -> PERes<()> {
        Ok(WriteBytesExt::write_i32::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_i64(&mut self, value: i64) -> PERes<()> {
        Ok(WriteBytesExt::write_i64::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_i128(&mut self, value: i128) -> PERes<()> {
        Ok(WriteBytesExt::write_i128::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_f32(&mut self, value: f32) -> PERes<()> {
        Ok(WriteBytesExt::write_f32::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_f64(&mut self, value: f64) -> PERes<()> {
        Ok(WriteBytesExt::write_f64::<BigEndian>(self, value)?)
    }
}

impl<W: Write + ?Sized> WriteFormat for W {}

pub(crate) trait ReadFormat: Read {
    #[inline]
    fn read_u8(&mut self) -> PERes<u8> {
        Ok(ReadBytesExt::read_u8(self)?)
    }

    #[inline]
    fn read_u16(&mut self) -> PERes<u16> {
        Ok(ReadBytesExt::read_u16::<BigEndian>(self)?)
    }

    #[inline]
    fn read_u32(&mut self) -> PERes<u32> {
        Ok(ReadBytesExt::read_u32::<BigEndian>(self)?)
    }

    #[inline]
    fn read_u64(&mut self) -> PERes<u64> {
        Ok(ReadBytesExt::read_u64::<BigEndian>(self)?)
    }

    #[inline]
    fn read_u128(&mut self) -> PERes<u128> {
        Ok(ReadBytesExt::read_u128::<BigEndian>(self)?)
    }

    #[inline]
    fn read_i8(&mut self) -> PERes<i8> {
        Ok(ReadBytesExt::read_i8(self)?)
    }

    #[inline]
    fn read_i16(&mut self) -> PERes<i16> {
        Ok(ReadBytesExt::read_i16::<BigEndian>(self)?)
    }

    #[inline]
    fn read_i32(&mut self) -> PERes<i32> {
        Ok(ReadBytesExt::read_i32::<BigEndian>(self)?)
    }

    #[inline]
    fn read_i64(&mut self) -> PERes<i64> {
        Ok(ReadBytesExt::read_i64::<BigEndian>(self)?)
    }

    #[inline]
    fn read_i128(&mut self) -> PERes<i128> {
        Ok(ReadBytesExt::read_i128::<BigEndian>(self)?)
    }

    #[inline]
    fn read_f32(&mut self) -> PERes<f32> {
        Ok(ReadBytesExt::read_f32::<BigEndian>(self)?)
    }

    #[inline]
    fn read_f64(&mut self) -> PERes<f64> {
        Ok(ReadBytesExt::read_f64::<BigEndian>(self)?)
    }
}

impl<R: Read + ?Sized> ReadFormat for R {}
